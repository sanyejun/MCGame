﻿using UnityEngine;
using System.Collections;
using PathologicalGames;

public class Brick : MonoBehaviour {
    float speed = 2;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        BrickMove();

        if (transform.position.z < -10)
        {
            Destroy(this.gameObject);
        }
	}

    void BrickMove()
    {
        transform.position -= transform.forward * speed * Time.deltaTime;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            //Destroy(other.transform.parent.gameObject);
            PoolManager.Pools["bullet"].Despawn(other.transform.parent.transform);
            Destroy(this.gameObject);
        }
    }


}
