﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// 计时器
/// </summary>
public class Timer : MonoBehaviour {

    public static Timer Instance;

    void Awake()
    {
        Instance = this;
    }

    public void DelayTimeToDo(float time, Action a)
    {
        StartCoroutine(TimeToDo(time, a));
    }

    IEnumerator TimeToDo(float time, Action a)
    {
        yield return new WaitForSeconds(time);
        a();
    }

}
