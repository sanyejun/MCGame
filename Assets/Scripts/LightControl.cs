﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightControl : MonoBehaviour {

    Light light;

	// Use this for initialization
	void Start () {
        light = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
        CircadianSystem();
    }

    /// <summary>
    /// 昼夜系统
    /// </summary>
    void CircadianSystem()
    {
        light.transform.rotation = Quaternion.Euler(-90 + System.DateTime.Now.Hour * 15, -30, 0);
    }
}
