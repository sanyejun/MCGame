﻿using UnityEngine;
using System.Collections;

public class BallControl : MonoBehaviour {

    public float speed = 10.0f;
    private Vector3 dir = Vector3.zero;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        BallMove();
        BallInputMove();
	}

    /// <summary>
    /// 重力感应移动
    /// </summary>
    void BallMove()
    {
        dir.x = Input.acceleration.x;
        dir.z = Input.acceleration.y;
        this.GetComponent<Rigidbody>().AddForce(dir.x * speed, 0, dir.z * speed);
    }

    /// <summary>
    /// 输入移动
    /// </summary>
    void BallInputMove()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        this.GetComponent<Rigidbody>().AddForce(h * speed, 0, v * speed);
    }
}
