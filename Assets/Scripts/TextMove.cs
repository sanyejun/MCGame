﻿using UnityEngine;
using System.Collections;
using PathologicalGames;

public class TextMove : MonoBehaviour {

    public float speed = 5;

	// Use this for initialization
	public void Start () {
        StartCoroutine(DestroyBullet());
	}
	
	// Update is called once per frame
	void Update () {
        Move(transform.forward);
	}

    void Move(Vector3 dir)
    {
        transform.position += dir * speed * Time.deltaTime;
    }

    IEnumerator DestroyBullet()
    {
        yield return new WaitForSeconds(3);
        //返回池中
        PoolManager.Pools["bullet"].Despawn(this.transform);
    }
}
