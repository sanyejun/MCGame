﻿using UnityEngine;
using System.Collections;
using PathologicalGames;

public class ShotControl : MonoBehaviour {

    Mir mic;
    public SpawnPool spawnPool;
    public GameObject bullet;
    public float soundCD;
    public bool isShot = true;

	// Use this for initialization
	void Start () {
        mic = GetComponent<Mir>();
        spawnPool = PoolManager.Pools["bullet"];
	}
	
	// Update is called once per frame
	void Update () {
        if (mic.sound > 1 && isShot)
        {
            Transform go = spawnPool.Spawn("Text");
            go.transform.position = transform.position;
            go.GetComponent<TextMove>().Start();
            isShot = false;
            StartCoroutine(ReduceCD());
        }
	}

    IEnumerator ReduceCD()
    {
        Debug.Log("执行协程");
        yield return new WaitForSeconds(0.5f);
        isShot = true;
        //StopCoroutine(ReduceCD());
    }

}
