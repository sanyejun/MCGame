﻿using UnityEngine;
using System.Collections;

/// <summary>
/// -4----4
/// </summary>
public class CreateBrick : MonoBehaviour {

    public GameObject brick;
    Vector3 brickPos = new Vector3(0, 0.5f, 13);

	// Use this for initialization
	void Start () {
        CreateTheBrick();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// 创建砖块
    /// </summary>
    void CreateTheBrick()
    {
        int num = Random.Range(-4, 4);
        Debug.Log("随机数" + num);
        Instantiate(brick, new Vector3(num, brickPos.y, brickPos.z), Quaternion.identity);

        Timer.Instance.DelayTimeToDo(2, CreateTheBrick);
    }
}
